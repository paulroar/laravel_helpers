@if(RoarIT\Models\SocialSite::active()->count())
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                @foreach(RoarIT\Models\SocialSite::active()->get() as $site)
                    <div class="col-md-6 col-lg-4" style="margin-bottom:10px;">
                        <a
                            class="btn btn-block btn-social btn-{{ $site->class }}"
                            href="{{ route('auth.social.redirect', $site ) }}"
                        >
                            <span class="fa fa-{{ $site->button }}"></span>
                            Login with {{ $site->name }}
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
