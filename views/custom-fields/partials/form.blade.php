
@isset($availableCustomFields)
    @foreach($availableCustomFields as $customField)
        @include('partials.form.' . $customField['input'], ['field' => $customField['field'], 'required' => false, 'label' => $customField['label']])
    @endforeach
@endisset
