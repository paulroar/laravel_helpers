
@isset($availableCustomFields)
    @foreach($availableCustomFields as $customField)
        <th>
            @component('table_filter', ['field' => $field = $customField['field']])
                <input class="form-control" value="{{ request($field) }}" name="{{ $field }}">
            @endcomponent 
        </th>
    @endforeach
@endisset
