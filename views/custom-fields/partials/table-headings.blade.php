
@isset($availableCustomFields)
    @foreach($availableCustomFields as $customField)
        <th>{{ __($customField['label']) }}</th>
    @endforeach
@endisset
