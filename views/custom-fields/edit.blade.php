@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <span>{{ __('Custom Fields') }} - {{ $custom_field->model }}</span>

                </div>

                <div class="card-block">
                    <div class="container">
                        <form class="mt-3" action="{{ route('roar-it::custom-fields.update', [$custom_field]) }}" method="post">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            @foreach (\RoarIT\Models\CustomField::fieldCounts() as $type => $count)
                                <div class="row">
                                    @for ($i=0; $i < $count; $i++)
                                        @include('partials.form.text', ['model' => $custom_field, 'field' => "custom_${type}_${i}", 'wrappingClass' => "col-md-6"])
                                    @endfor
                                </div>
                                <hr>
                            @endforeach
                            
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                                </div>
                            </div>
                        </form>    
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
