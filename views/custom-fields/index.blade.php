@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <span>{{ __('Custom Fields') }}</span>
                </div>

                <div class="card-block">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Model') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($custom_field_labels as $customField)
                                <tr>
                                    <td>{{ $customField->model }}</td>
                                    <td>@include('partials.edit', ['model' => $customField])</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @paginate($custom_field_labels)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
