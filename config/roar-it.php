<?php

use RoarIT\Models\CustomField;
use RoarIT\Models\CustomFieldLabel;

return [
    'packages' => [
        'auditable' => false,
        'social_login' => false,
        'tokens' => false,
        'custom_fields' => false,
    ],
    'per_page' => 25,
    'custom_fields' => [
        'route' => [
            'middleware' => [],
            'enabled' => true,
        ],
        'authorize' => false,
        'field_counts' => [
            'timestamp' => 5,
            'boolean' => 5,
            'decimal' => 5,
            'integer' => 5,
            'string' => 5,
            'text' => 5,
        ],
        'tables' => ['custom_fields', 'custom_field_labels'],
        'table_callbacks' => [
            'custom_fields' => [CustomField::class, 'createColumn'],
            'custom_field_labels' => [CustomFieldLabel::class, 'createColumn'],
        ],
        'inputs' => [
            'timestamp' => 'datepicker',
            'boolean' => 'checkbox',
        ],
        'database-date-format' => 'Y-m-d H:i:s',
        'models' => [
            'CustomField' => CustomField::class,
            'CustomFieldLabel' => CustomFieldLabel::class,
        ],
    ],
    'audit' => [
        'model' => 'RoarIT\\Models\\Audit',
        'database-date-format' => 'Y-m-d H:i:s',
    ],
];
