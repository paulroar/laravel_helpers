<?php
namespace RoarIT\Tokens;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cache;

class Token
{
    use SerializesModels;
    protected function expiresAt()
    {
        $lasts = $this->lastsFor();
        if ($lasts === 0) {
            return null;
        }
        return Carbon::now()->addMinutes($lasts);
    }
    public function lastsFor($lasts = null)
    {
        static $lastsFor = 0;
        if (!is_null($lasts)) {
            $lastsFor = $lasts;
        }
        return $lastsFor;
    }

    private function buildData() : array
    {
        return [
            'expires' => optional($this->expiresAt())->getTimestamp() ?? false,
            'data' => serialize($this),
        ];
    }
    final public function __toString()
    {
        return tap(Crypt::encryptString(json_encode($this->buildData())), function ($key) {
            Cache::put([$key => 1], $this->expiresAt() ?? (24 * 60));
        });
    }
    final public static function from($token) : Token
    {
        try {
            $content = json_decode(Crypt::decryptString($token));
        } catch (DecryptException $e) {
            throw new \Exception("Invalid token URL, please check that you have copied it correctly");
        }

        if ($content->expires) {
            $expires = Carbon::createFromTimestamp($content->expires);
            if ($expires->lt(Carbon::now()) || !Cache::has($token)) {
                throw new \Exception("Token Expired");
            }
        }

        Cache::increment($token);

        return unserialize($content->data);
    }
    public static function handle($token)
    {
        static::fromToken($token)->process($content->data);
    }

    public function url()
    {
        return route('roar-it.token.process', [$this->__toString()], true);
    }

    public function process()
    {
        throw new \Exception("Child class needs to override the process method");
    }
}
