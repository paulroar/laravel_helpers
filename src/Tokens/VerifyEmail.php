<?php
namespace RoarIT\Tokens;

class VerifyEmail extends Token
{
    private $user;
    private $email;

    public function __construct($user)
    {
        $this->user = $user;
        $this->email = $user->email;
        $this->lastsFor(60);
    }


    public function process()
    {
        if ($this->user->email != $this->email) {
            flash("This token has expired")->error();
            return redirect()->route('home');
        }

        tap($this->user, function ($user) {
            $user->email_verified = true;
            $user->save();
        });

        flash("Your email has been verified!");

        return redirect()->route('home');
    }
}
