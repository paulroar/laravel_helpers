<?php

namespace RoarIT\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use RoarIT\Traits\Filterable;

class Model extends BaseModel
{
    use Filterable;

    public static function findByRouteKey($search, $fail = true)
    {
        $method = $fail ? 'firstOrFail' : 'first';

        $model = new static;

        return $model->where($model->qualifyColumn($model->getRouteKeyName()), $search)->$method();
    }

    public function scopeJoinWithoutSelect($query, $table, $index, $operator = "=", $foreign)
    {
        return $query->leftJoin($table, $index, $operator, $foreign)
             ->select($this->getTable() . ".*")
             ->withCount($this->withCount);
    }

    public function getRouteKeyValue()
    {
        $field = $this->getRouteKeyName();
        return $this->$field;
    }

    public static function getTableName()
    {
        return (new static)->getTable();
    }

    public function viewUrl()
    {
        return route($this->getTable() . 'show', [$this]);
    }

    public function singularModelName()
    {
        return class_basename(get_class($this));
    }

    public function resolveRouteBinding($value)
    {
        return $this->where($this->qualifyColumn($this->getRouteKeyName()), $value)->first();
    }
}
