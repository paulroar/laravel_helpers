<?php

namespace RoarIT\Models;

use Illuminate\Database\Eloquent\Model;
use RoarIT\Traits\HasDefaultValues;

class SocialSite extends Model
{
    use HasDefaultValues;

    protected $fillable = ['name', 'class', 'button'];

    public function getRouteKeyName(){
        return 'class';
    }

    public function defaultValues(){
        return [
            'button' => $this->class,
        ];
    }

    public function scopeActive($query){
        return $query->where('active', true)->whereNotNull('app_id')->whereNotNull('app_secret');
    }

    public function logins(){
        return $this->hasMany(SocialLogin::class);
    }
}
