<?php

namespace RoarIT\Models;

use Illuminate\Database\Eloquent\Model;

class SocialLogin extends Model
{
    protected $fillable = ['provider_id', 'social_site_id', 'user_id'];

    public function user(){
        return $this->belongsTo(config('auth.providers.users.model'));
    }

    public function site(){
        return $this->belongsTo(SocialSite::class, 'social_site_id');
    }
}
