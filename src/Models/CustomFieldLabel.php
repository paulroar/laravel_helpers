<?php

namespace RoarIT\Models;

use RoarIT\Traits\HasCustomFields;
use RoarIT\Traits\Validatable;
use RoarIT\Utils\ClassUtils;

class CustomFieldLabel extends Model
{
    use Validatable;
    
    public function getFillable()
    {
        return resolve(CustomField::class)->getFillable();
    }

    public static function seed()
    {
        static::unguard();

        ClassUtils::getClassesWhichUseTrait(HasCustomFields::class)->each(function ($class) {
            $label = CustomFieldLabel::firstOrCreate([
                'model' => $class,
            ]);
        });

        static::reguard();
    }

    public function routeBase()
    {
        return 'roar-it::custom-fields';
    }

    public function validation()
    {
        return collect($this->getFillable())->mapWithKeys(function ($field) {
            return [$field => ['nullable', 'string', 'max:191']];
        })->toArray();
    }

    protected static function createColumn($table, $type, $index)
    {
        $table->string("custom_${type}_${index}")->nullable();
    }

    public function getDateFormat()
    {
        return config("roar-it.custom_fields.database-date-format", parent::getDateFormat());
    }
}
