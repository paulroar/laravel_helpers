<?php

namespace RoarIT\Models;

use Illuminate\Database\Eloquent\Model;

class AuditLine extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function audit()
    {
        return $this->belongsTo(Audit::class);
    }

    public function getFieldIsVisibleAttribute($original)
    {
        $this->loadMissing('audit.auditable');

        if (is_null($this->audit->auditable)) {
            // if the model has been soft-deleted all fields will be
            // hidden until the model is restored
            return false;
        }

        return ! in_array($this->field_name, $this->audit->auditable->getHidden());
    }

    public function getDateFormat()
    {
        return config("roar-it.audit.database-date-format", parent::getDateFormat());
    }
}
