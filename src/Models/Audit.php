<?php

namespace RoarIT\Models;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $guarded = [];

    public function lines()
    {
        return $this->hasMany(AuditLine::class);
    }

    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'));
    }

    public function auditable()
    {
        return $this->morphTo('auditable');
    }

    public function getDateFormat()
    {
        return config("roar-it.audit.database-date-format", parent::getDateFormat());
    }
}
