<?php

namespace RoarIT\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use RoarIT\Traits\HasCompositePrimaryKey;
use RoarIT\Traits\HasCustomFields;
use RoarIT\Traits\Validatable;
use RoarIT\Utils\ClassUtils;

class CustomField extends Model
{
    use HasCompositePrimaryKey, Validatable;

    protected $primaryKey = ['model_type', 'model_id'];
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
    }

    const FIELD_TYPES = ['boolean', 'timestamp', 'decimal', 'integer', 'string', 'text'];

    public static function fieldCounts()
    {
        static $res = null;

        if (! is_null($res)) {
            return $res;
        }

        $current = static::currentFieldCounts();
        $desired = static::desiredFieldCounts();

        $res = [];

        foreach ($desired as $type => $count) {
            $current_count = $current[$type] ?? 0;

            $res[$type] = $count > $current_count ? $current_count : $count;
        }

        return $res;
    }

    protected static function createColumn($table, $type, $index)
    {
        $table->$type("custom_${type}_${index}")->nullable();
    }

    public static function seedColumnsForAllTables($tables = null)
    {
        $tables = $tables ?? config('roar-it.custom_fields.tables', []);

        foreach ($tables as $table) {
            static::seedDesiredColumns($table);
        }
    }

    public static function seedDesiredColumns($table, $callback = null)
    {
        $callback = $callback ?? config("roar-it.custom_fields.table_callbacks.$table", [static::class, 'createColumn']);

        $current = static::currentFieldCounts([$table]);
        $desired = static::desiredFieldCounts();

        $required = [];

        foreach ($desired as $type => $count) {
            $current_count = $current[$type] ?? 0;

            if (($count - $current_count) > 0) {
                $required[$type] = [
                    'current' => $current_count,
                    'required' => $count,
                ];
            }
        }
        
        Schema::table($table, function ($t) use ($required, $callback) {
            foreach ($required as $type => $counts) {
                for ($i = $counts['current']; $i < $counts['required']; $i++) {
                    $callback($t, $type, $i);
                }
            }
        });
    }

    private static function currentFieldCounts($tables = null)
    {
        $table = $tables[0] ?? 'custom_fields';
        $results = \DB::connection(config('roar-it.custom_fields.information_schema', 'information_schema'))->select("
SELECT 
    substring(COLUMN_NAME, 8, locate('_', COLUMN_NAME, 8) - 8) AS 'type',
    count(1) AS count
FROM COLUMNS 
WHERE 
    TABLE_NAME = ? AND 
    COLUMN_NAME LIKE 'custom_%' 
GROUP BY substring(COLUMN_NAME, 8, locate('_', COLUMN_NAME, 8) - 8)", [$table]);

        $res = [];

        foreach ($results as $result) {
            $res[$result->type] = $result->count;
        }

        return $res;
    }

    private static function desiredFieldCounts()
    {
        return array_only(config('roar-it.custom_fields.field_counts', []), static::FIELD_TYPES);
    }

    public function getFillable()
    {
        $fillable = [];

        foreach (static::fieldCounts() as $type => $count) {
            for ($i=0; $i < $count; $i++) {
                $fillable[] = "custom_${type}_${i}";
            }
        }

        return $fillable;
    }

    public function getCasts()
    {
        $casts = [];

        $mapping = [
            'timestamp' => 'datetime',
            'decimal' => 'real',
        ];

        foreach (static::fieldCounts() as $type => $count) {
            for ($i=0; $i < $count; $i++) {
                $casts["custom_${type}_${i}"] = $mapping[$type] ?? $type;
            }
        }

        return $casts;
    }


    public function validation()
    {
        $validation = [];

        $dateFormat = $this->getDateFormat();

        $mapping = [
            'timestamp' => "carbon_date:$dateFormat",
            'decimal' => 'numeric',
            'text' => 'string',
        ];

        foreach (static::fieldCounts() as $type => $count) {
            for ($i=0; $i < $count; $i++) {
                $validation["custom_${type}_${i}"] = ['nullable', $mapping[$type] ?? $type];
            }
        }

        return $validation;
    }

    public function setAttribute($key, $value)
    {
        if (str_contains($key, 'timestamp')) {
            // invalid dates will cause errors before it gets to the database
            validator([$key => $value], [$key => ['carbon_date:' . $this->getDateFormat()]])->validate();
        }
        return parent::setAttribute($key, $value);
    }

    public static function modelsWithCustomFields()
    {
        return ClassUtils::getClassesWhichUseTrait(HasCustomFields::class);
    }

    public function getDates()
    {
        return collect($this->getCasts())->filter(function ($value, $key) {
            return $value === "datetime";
        })->keys()->toArray();
    }

    public function getDateFormat()
    {
        return config("roar-it.custom_fields.database-date-format", parent::getDateFormat());
    }
}
