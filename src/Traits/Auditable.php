<?php

namespace RoarIT\Traits;

use RoarIT\Models\Audit;
use RoarIT\Models\AuditLine;
use RoarIT\Utils\ClassUtils;

trait Auditable
{
    public $audit_modifier = null;
    public $audit_message = null;

    public static function bootAuditable()
    {
        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->storeAudit($event);
            });
        }
    }

    protected static function getModelEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }
        return [
            'created', 'updated', 'deleted',// 'restored',
        ];
    }


    protected function getEventName($model, $action, $changes = [])
    {
        $name = $this->getShortClassName();
        $change = '';
        if ($action == 'updated') {
            $count = count($changes);
            if ($count == 1 || ($count == 2 && array_key_exists('updated_at', $changes))) {
                $change = "_{$changes[0]}";
            }
        }
        return "{$action}_{$name}{$change}";
    }


    private function getShortClassName()
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }

    public function storeAudit($event)
    {
        $modifier = $this->audit_modifier ?? $this;
        $message = $this->audit_message ?? null;
        
        $changed = $this->getDirtyFieldsForAuditTrail();
        $before = $this->getOriginalValuesForFields(array_keys($changed));

        $audit = $this->audits()->create([
            'user_id' => auth()->id(),
            'modifier_id' => $modifier->id,
            'modifier_type' => get_class($modifier),
            'message' => $message,
            'event_name' => $this->getEventName($this, $event, $fields = array_keys($changed)),
        ]);

        foreach ($fields as $field) {
            if ('updated_at' === $field) {
                continue;
            }

            $mutator = 'get' . studly_case($field) . 'Attribute';

            $mutatable = method_exists($this, $mutator);

            $old = $mutatable && isset($before[$field]) ? $this->$mutator($before[$field]) : $before[$field] ?? null;
            $new = $mutatable && isset($changed[$field]) ? $this->$mutator($changed[$field]) : $changed[$field];

            if ($old === $new) {
                continue;
            }

            $audit->lines()->create([
                'field_name' => $field,
                'old_value' => $old,
                'new_value' => $new,
            ]);
        }

        if ($audit->lines()->count() == 0) {
            $audit->delete();
        }
    }

    public function audits()
    {
        return $this->morphMany(config('roar-it.audit.model', Audit::class), 'auditable');
    }

    public function getDirtyFieldsForAuditTrail()
    {
        $res = $this->getDirty();

        foreach (ClassUtils::getTraitMethodNames(get_class($this), 'getDirtyFieldsForAuditTrail') as $method) {
            $res = array_merge_recursive($this->$method(), $res);
        }

        return $res;
    }

    public function getOriginalValuesForFields(array $fields)
    {
        $res = array_intersect_key($this->original, array_flip($fields));

        foreach (ClassUtils::getTraitMethodNames(get_class($this), 'getOriginalValuesForFields') as $method) {
            $res = array_merge_recursive($this->$method($fields), $res);
        }

        return $res;
    }
}
