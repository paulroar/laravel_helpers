<?php 

namespace RoarIT\Traits;

trait HasBitField
{
    protected $booleanAttributeValues = [];

    public static function bootHasBitField()
    {
        static::retrieved(function ($model) {
            $model->parseBitfield();
        });

        static::saving(function ($model) {
            $model->buildBitfield();
        });
    }

    public function getAttribute($key)
    {
        if ($key === $this->bitField) {
            return $this->booleanAttributeValues;
        }

        if (in_array($key, $this->booleans)) {
            return $this->booleanAttributeValues[$key];
        }

        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->booleans)) {
            return $this->booleanAttributeValues[$key] = !! $value;
        } else {
            return parent::setAttribute($key, $value);
        }
    }

    public function parseBitfield()
    {
        $data = preg_replace("/[^0-9A-Fa-f]/", "", $this->attributes[$this->bitField]);
        $binary = base_convert($data, 16, 2);

        $i = strlen($binary);
        foreach ($this->booleans as $bool) {
            $i--;
            if ($i < 0) {
                break;
            }
            $this->$bool = $binary[$i] === '1';
        }
    }

    public function buildBitfield($values = null)
    {
        return $this->attributes[$this->bitField] = static::buildBitfieldString($this->booleans, $values ?? $this->booleanAttributeValues);
    }

    public static function buildBitfieldString($booleans = [], $values = [])
    {
        $binary = "";

        foreach ($booleans as $bool) {
            $value = $values[$bool] ?? false;
            $binary = ($value ? '1' : '0') . $binary;
        }

        return base_convert($binary, 2, 16);
    }

    public function scopeWhereBitfield($query, $flags)
    {
        $value = $this->buildBitfield($flags);
        $field = $this->bitField;
        return $query->whereRaw("(conv($field, 16, 10) & conv(?, 16, 10)) >= ?", [$value, $value]);
    }

    public function scopeWhereHasFlags($query, $flags)
    {
        $values = collect($flags)->mapWithKeys(function ($flag) {
            return [$flag => true];
        })->toArray();
        return $query->whereBitfield($values);
    }
}
