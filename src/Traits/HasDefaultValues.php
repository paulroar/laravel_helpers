<?php 

namespace RoarIT\Traits;

use Closure;
use RoarIT\Utils\ClassUtils;

trait HasDefaultValues
{
    protected static function bootHasDefaultValues()
    {
        static::creating(function ($model) {
            $model->setDefaultValues();
        });
    }

    public function setDefaultValues()
    {
        foreach ($this->mergedDefaultValues() as $field => $value) {
            if (is_null($this->$field)) {
                $this->$field = ($value instanceof Closure) ? $value() : $value;
            }
        }
    }

    protected function defaultValues()
    {
        return [];
    }

    public function mergedDefaultValues()
    {
        $values = $this->defaultValues();

        foreach (ClassUtils::getTraitMethodNames(get_class($this), 'defaultValues') as $method) {
            $values = array_merge($this->$method(), $values);
        }

        return $values;
    }
}
