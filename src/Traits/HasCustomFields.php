<?php

namespace RoarIT\Traits;

use Illuminate\Support\Facades\Schema;
use RoarIT\Models\CustomField;
use RoarIT\Models\CustomFieldLabel;

trait HasCustomFields
{
    private $_customFields = null;

    protected static function bootHasCustomFields()
    {
        if (Schema::hasTable('custom_fields')) {
            $class = static::class;
            $model = new static;

            static::addGlobalScope('custom-fields', function ($query) use ($class, $model) {
                if (! config('roar-it.packages.custom_fields')) {
                    return $query;
                }
                return $query
                // ->leftJoin('custom_fields', function ($join) use ($class, $model) {
                //     $join->on('custom_fields.model_id', $model->qualifyColumn('id'))
                //              ->where('custom_fields.model_type', $class);
                // })
                ->with('customFieldsRelationship');
                // cant remember why this was overriding the select but it breaks withCount
                // ->select($model->qualifyColumn('*'));
            });

            static::retrieved(function ($model) {
                $model->_customFields = $model->customFieldsRelationship;
            });

            static::saved(function ($model) {
                if (config('roar-it.packages.custom_fields')) {
                    $custom = $model->custom_fields;
                    $custom->model_id = $model->id;
                    $custom->save();
                }
            });
        }
    }

    public function customFieldsRelationship()
    {
        return $this->hasOne(config("roar-it.custom_fields.models.CustomField"), 'model_id')->where('model_type', get_class($this));
    }

    public function getCustomFieldsAttribute()
    {
        if (is_null($this->_customFields)) {
            if ($this->id && isset($this->customFieldsRelationship)) {
                $this->_customFields = $this->customFieldsRelationship;
            } else {
                $this->_customFields = resolve(config("roar-it.custom_fields.models.CustomField"));
                $this->_customFields->model_type = get_class($this);
                $this->_customFields->model_id = $this->id;
            }
        }

        return $this->_customFields;
    }

    public function getAttribute($key)
    {
        if ($key !== 'custom_fields' && starts_with($key, 'custom_') && config('roar-it.packages.custom_fields')) {
            return $this->custom_fields->$key;
        }
        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if ($key !== 'custom_fields' && starts_with($key, 'custom_') && config('roar-it.packages.custom_fields')) {
            return $this->custom_fields->$key = $value;
        }
        return parent::setAttribute($key, $value);
    }

    public function fill(array $attributes)
    {
        if (config('roar-it.packages.custom_fields')) {
            $fields = $this->custom_fields->getFillable();

            if (!empty($fields)) {
                $this->custom_fields->fill($attributes);
            }
        }

        return parent::fill($attributes);
    }

    public function getDirtyFieldsForAuditTrailHasCustomFields()
    {
        if (! config('roar-it.packages.custom_fields')) {
            return [];
        }

        if (Schema::hasTable('custom_fields')) {
            return $this->custom_fields->getDirty();
        }

        return [];
    }

    public function getOriginalValuesForFieldsHasCustomFields(array $fields)
    {
        return array_intersect_key($this->custom_fields->original, array_flip($fields));
    }

    public static function availableCustomFields()
    {
        if (! config('roar-it.packages.custom_fields')) {
            return [];
        }

        $customFieldLabelsClass = config("roar-it.custom_fields.models.CustomFieldLabel");
        $labels = $customFieldLabelsClass::where('model', static::class)->first();

        if (is_null($labels)) {
            return [];
        }

        $res = [];

        $mapping = [

        ];

        $inputs = config('roar-it.custom_fields.inputs');

        $customFieldClass = config("roar-it.custom_fields.models.CustomField");

        foreach ($customFieldClass::fieldCounts() as $type => $count) {
            for ($i=0; $i < $count; $i++) {
                $field = "custom_${type}_${i}";
                if (isset($labels->$field)) {
                    $res[] = [
                        'field' => $field,
                        'type' => $mapping[$type] ?? $type,
                        'input' => $inputs[$type] ?? $type,
                        'label' => $labels->$field,
                    ];
                }
            }
        }

        return $res;
    }

    public function getDates()
    {
        $res = parent::getDates();

        if (config('roar-it.packages.custom_fields')) {
            $res = array_merge($res, $this->custom_fields->getDates());
        }

        return $res;
    }

    public function validationHasCustomFields()
    {
        if (config('roar-it.packages.custom_fields')) {
            return $this->custom_fields->rules();
        }

        return [];
    }

    public function isDirty($attributes = null)
    {
        $attributes = is_array($attributes) ? $attributes : func_get_args();

        if ($this->custom_fields->isDirty($attributes)) {
            $this->setUpdatedAt($this->freshTimestamp());
        }

        $res = parent::isDirty($attributes);

        return $res;
    }
}
