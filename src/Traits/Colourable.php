<?php 

namespace RoarIT\Traits;

trait Colourable {

    public static function bootColourable(){
        static::creating(function($model){
            $model->setColour();
        });
    }

    public function randomColour() {
        $HexValues = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
        $colour = '#';
        for ($i = 0; $i < 6; $i++ ) {
            $position = rand ( 0 , 15);
            $colour .= $HexValues[ $position];
        }
        return $colour;
    }
    
    public function setColour($colour = null){
        $this->colour = $colour ?? ($this->colour ?? $this->randomColour());        
    }
}
