<?php

namespace RoarIT\Traits;

use RoarIT\Filters\Filters;
use RoarIT\Utils\ClassUtils;

trait Filterable
{
    public function getSortableFields()
    {
        $res = $this->sortable ?? [];

        foreach (ClassUtils::getTraitMethodNames(get_class($this), 'getSortableFields') as $method) {
            $res = $this->$method($fields) + $res;
        }

        return $res;
    }


    public function scopeFilter($query, $filter = null)
    {
        // Allowing for 'Model::filter()->paginate()' rather than always having to
        // call 'Model::filter(new ModelFilter(request()->all())->paginate()'

        $args = ($array = is_array($filter)) ? $filter : request()->all();
        
        if (method_exists($this, 'defaultFilters')) {
            $args = array_merge($this->defaultFilters(), $args);
        }

        if ($array) {
            $filter = null;
        }

        if (is_null($filter)) {
            if (method_exists($this, 'getFilterClass')) {
                $filterName = $this->getFilterClass();
            } else {
                $modelName = class_basename(get_class($this));
                $filterName = "App\\Filters\\{$modelName}Filter";
            }

            if (class_exists($filterName)) {
                $filter = new $filterName($args);
            }
        }

        $filter = $filter ?? new Filters($args);
        return $filter->_apply($query);
    }
}
