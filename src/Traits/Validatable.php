<?php

namespace RoarIT\Traits;

use RoarIT\Utils\ClassUtils;
use Validator;

trait Validatable
{
    public function validation()
    {
        throw new \Exception('No validation rules set');
    }

    public function rules()
    {
        $existing_rules = $this->validation();
        $rules =[];
        foreach ($existing_rules as $field => $fieldRules) {
            $fieldRules = is_array($fieldRules) ? $fieldRules : explode("|", $fieldRules);
            $rules[$field] = $fieldRules;
        }
        foreach (ClassUtils::getTraitMethodNames(get_class($this), 'validation') as $method) {
            $rules = array_merge_recursive($this->$method(), $rules);
        }

        return $rules;
    }

    public function validate()
    {
        $validation = $this->rules();

        $attributes = [];

        foreach ($validation as $field => $value) {
            $topLevel = explode('.', $field)[0];
            $attributes[$topLevel] = $this->$topLevel ?? null;
        }

        $attributes['raw'] = $this->getAttributes();

        Validator::make($attributes, $validation)->validate();
    }

    protected static function bootValidatable()
    {
        static::saving(function ($model) {
            if (method_exists($model, 'setDefaultValues')) {
                $model->setDefaultValues();
            }

            $model->validate();
        });
    }

    public function validationHash()
    {
        $result = [];

        foreach ($this->rules() as $field => $rules) {
            $fieldRules = [];

            foreach ((is_array($rules) ? $rules : explode("|", $rules)) as $rule) {
                if (is_object($rule)) {
                    $fieldRules[get_class($rule)][] = $rule;
                }

                if (is_string($rule)) {
                    $tokens = explode(":", $rule);
                    $fieldRules[$tokens[0]] = explode(",", last($tokens));
                }
            }

            $result[$field] = $fieldRules;
        }

        return $result;
    }
}
