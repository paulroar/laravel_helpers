<?php

namespace RoarIT\Filters;

class Filters
{
    protected $args = [];

    protected function getSortableFields($query)
    {
        $model = $query->getModel();

        return optional($model)->getSortableFields() ?? [];
    }

    public function __construct($args = null)
    {
        $this->args = $args ?? request()->all();
    }

    public function _apply($query)
    {
        foreach ($this->args as $key => $value) {
            if (method_exists($this, $key) && !starts_with($key, '_')) {
                $query = $this->$key($query, $value);
            }
        }


        return $query;
    }
    
    protected function _join_required($query, $sort_field)
    {
        return $query;
    }

    public function sort($query, $field)
    {
        $sortable = $this->getSortableFields($query);
        $fields = explode(',', $field);
        foreach ($fields as $sort_field) {
            if (in_array($sort_field, $sortable)) {
                $query = $this->_join_required($query, $sort_field)->orderBy($sort_field);
            }
        }
        return $query;
    }

    public function sort_desc($query, $field)
    {
        $sortable = $this->getSortableFields($query);
        $fields = explode(',', $field);
        foreach ($fields as $sort_field) {
            if (in_array($sort_field, $sortable)) {
                $query = $this->_join_required($query, $sort_field)->orderByDesc($sort_field);
            }
        }
        return $query;
    }
}
