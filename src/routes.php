<?php

if (config("roar-it.packages.tokens")) {
    Route::middleware('web')->get('tokens/{token}', 'RoarIT\Controllers\TokenController@process')->name('roar-it.token.process');
    Route::middleware('web')->post('tokens/{token}', 'RoarIT\Controllers\TokenController@process')->name('roar-it.token.process');
}

if (config("roar-it.packages.social_login")) {
    Route::group(['prefix' => 'auth/social', 'middleware' => 'web'], function () {
        Route::post('details', 'RoarIT\Controllers\SocialAuthController@setUserDetails');
        Route::get('details', 'RoarIT\Controllers\SocialAuthController@getUserDetailsForm');
        Route::get('connect', ['uses' => 'RoarIT\Controllers\SocialAuthController@getConnectPage', 'as' => 'auth.social.connect', 'middleware' => ['auth']]);
        Route::get('{site}', ['uses' => 'RoarIT\Controllers\SocialAuthController@redirectToProvider', 'as' => 'auth.social.redirect']);
        Route::get('{site}/callback', ['uses' => 'RoarIT\Controllers\SocialAuthController@handleProviderCallback', 'as' => 'auth.social.callback']);
    });
}

if (config("roar-it.packages.custom_fields") && config('roar-it.custom_fields.route.enabled')) {
    Route::group(['middleware' => config('roar-it.custom_fields.route.middleware'), 'as' => 'roar-it::'], function () {
        Route::resource('custom-fields', 'RoarIT\Controllers\CustomFieldLabelController');
    });
}
