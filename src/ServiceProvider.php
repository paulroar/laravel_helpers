<?php

namespace RoarIT;

use Carbon\CarbonInterface;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Carbon;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/roar-it.php' => config_path('roar-it.php'),
        ], 'config');

        $this->loadRoutesFrom(__DIR__.'/routes.php');

        static $packagesWithMigrations = [
            'auditable',
            'social_login',
            'custom_fields',
        ];


        foreach ($packagesWithMigrations as $package) {
            if (config("roar-it.packages.$package")) {
                $this->loadMigrationsFrom(__DIR__."/../database/migrations/$package/");
            }
        }



        Blade::directive('paginate', function ($paginator) {
            return "<?php echo e(($paginator)->appends(request()->except('page'))->links()); ?>";
        });

        Blade::directive('sortIcon', function ($col) {
            return "<?php
            if (request('sort') == $col) {
                echo \"<i class='fa fa-sort-asc' aria-hidden='true'></i>\";
            } elseif (request('sort_desc')==($col)) {
                echo \"<i class='fa fa-sort-desc' aria-hidden='true'></i>\";
            } else {
                echo \"<i class='fa fa-sort' aria-hidden='true'></i>\";
            }
            ?>";
        });

        View::share('sortableTableLink', function ($route, $field, $model = null) {
            $desc = request('sort') == $field;
            return route(
                $route,
                array_merge(
                    is_null($model) ? [] : (is_array($model) ? $model : [$model]),
                    request()->except(['page', 'sort', 'sort_desc']),
                    $desc ? ['sort_desc' => $field] : ['sort' => $field]
                )
            );
        });

        Validator::extend('carbon_date', function ($attribute, $value, $parameters, $validator) {
            if ($value instanceof CarbonInterface) {
                return true;
            }
            
            try {
                if ($parameters[0] ?? false) {
                    Date::createFromFormat($parameters[0], $value);
                } else {
                    Date::parse($value);
                }
                return true;
            } catch (\Exception $e) {
            }

            return false;
        }, "Invalid DateTime");

        $this->loadViewsFrom(__DIR__.'/../views', 'roar-it');

        $this->publishes([
            __DIR__.'/../views' => resource_path('views/vendor/roar-it'),
        ], 'views');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/roar-it.php',
            'roar-it'
        );
    }
}
