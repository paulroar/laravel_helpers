<?php

namespace RoarIT\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Laravel\Socialite\Contracts\Factory as Socialite;

use RoarIT\Models\SocialSite;
use RoarIT\Models\SocialLogin;

class SocialAuthController extends Controller
{
    private function setConfig(SocialSite $site)
    {
        config()->set("services.{$site->class}", [
            'client_id'     => $site->app_id,
            'client_secret' => $site->app_secret,
            'redirect'      => route('auth.social.callback', $site),
        ]);
    }

    public function redirectToProvider(SocialSite $site)
    {
        $this->setConfig($site);

        return app(Socialite::class)->driver($site->class)->redirect();
    }

    public function handleProviderCallback(SocialSite $site)
    {
        $this->setConfig($site);

        $user = app(Socialite::class)->driver($site->class)->user();

        $social = $site->logins()->where('provider_id', $user->id)->first();

        if ($social && $social->exists()) {
            return $this->loginUser($social->user);
        }

        if (auth()->check()) {
            $social = $site->logins()->create([
                'user_id'        => auth()->id(),
                'provider_id'    => $user->id,
            ]);
            flash("Succesfully connected with your $site->name account.")->success()->important();
            return redirect('/');
        }

        session([
            'user_data'       => $user,
            'social_provider' => $site->id,
        ]);

        return redirect('/auth/social/details');
    }

    public function getUserDetailsForm()
    {
        $user = session('user_data');

        $site = SocialSite::find(session('social_provider'));

        return view(
            'roar-it::social_site.social',
            [
                'name'     => $user->nickname,
                'email'    => $user->email,
                'provider' => $site,
            ]
        );
    }

    public function setUserDetails(Request $request)
    {
        $this->validate($request, $this->userValidation());

        $credentials = [
            'name'     => $request->input('name'),
            'email'    => $request->input('email'),
        ];

        if (strlen($request->input('password')) >= 1) {
            $credentials['password'] = bcrypt($request->input('password'));
        }

        $user = $this->registerUser($credentials);

        if (!$user || !$user->exists()) {
            return redirect()->back();
        }

        $social = SocialLogin::create([
            'user_id'        => $user->id,
            'social_site_id' => session('social_provider'),
            'provider_id'    => session('user_data')['id'],
        ]);

        auth()->login($user);

        session()->forget('user_data');
        session()->forget('social_provider');

        return redirect()->intended('/');
    }

    public function getConnectPage()
    {
        return view('roar-it::social_login.connect');
    }

    protected function userValidation()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

    protected function registerUser($credentials)
    {
        return resolve(config('auth.providers.users.model'))->create($credentials);
    }

    protected function loginUser($user)
    {
        auth()->login($user);
        return redirect('/');
    }
}
