<?php

namespace RoarIT\Controllers;

use Illuminate\Http\Request;

abstract class CrudController extends \App\Http\Controllers\Controller
{
    protected function templateBase()
    {
        return resolve($this->modelClass())->getTable();
    }

    protected function modelClass()
    {
        return 'App\\' . $this->shortModelClass();
    }

    protected function resourceClass()
    {
        return 'App\\Http\\Resources\\' . $this->shortModelClass();
    }

    protected function shortModelClass()
    {
        return str_before(class_basename(get_class($this)), 'Controller');
    }

    protected function eagerLoad()
    {
        return [];
    }

    protected function with($method)
    {
        $res = [];

        if (method_exists($class = $this->modelClass(), 'availableCustomFields')) {
            $res['availableCustomFields'] = $class::availableCustomFields();
        }

        return $res;
    }

    protected function model(bool $includeTrashed = false)
    {
        static $model = null;

        if (!is_null($model)) {
            return $model;
        }

        $key = request()->route()->parameters[$this->parameterName()] ?? null;

        $model = resolve($this->modelClass());

        if (!$key) {
            return $model;
        }

        return $model =
            $model
                ->when($includeTrashed && method_exists($model, 'withTrashed'), function ($query) {
                    return $query->withTrashed();
                })
                ->where($model->qualifyColumn($model->getRouteKeyName()), $key)
                ->with($this->eagerLoad())
                ->firstOrFail();
    }

    protected function parameterName()
    {
        return strtolower(class_basename($this->modelClass()));
    }

    protected function routeBase()
    {
        return $this->templateBase();
    }

    protected function requiresAuthorization($method)
    {
        return true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = $this->model();

        if ($this->requiresAuthorization('index')) {
            $this->authorize('index', $model);
        }

        $models = (
            method_exists($model, 'scopeFilter') ?
                        $model->filter() :
                        $model
                    )
                    ->with($this->eagerLoad())
                    ->paginate(config("roar-it.per_page"));

        if (request()->wantsJson()) {
            if (class_exists($resourceClass = $this->resourceClass())) {
                return $resourceClass::collection($models);
            }
            return $models;
        }

        return view($this->templateBase() . '.index', $this->with('index') ?? [])->with($model->getTable(), $models);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $model = $this->model();

        if ($this->requiresAuthorization('create')) {
            $this->authorize('create', $model);
        }

        return view($this->templateBase() . '.create', $this->with('create') ?? []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = $this->model();

        if ($this->requiresAuthorization('create')) {
            $this->authorize('create', $model);
        }

        $model = $model->create($request->all());

        if (request()->wantsJson()) {
            if (class_exists($resourceClass = $this->resourceClass())) {
                return new $resourceClass($model);
            }
            return $model;
        }

        flash($model->singularModelName()." created successfully")->success();

        if ($request->has('_redirect')) {
            return redirect($request->_redirect);
        }

        return redirect(route($this->routeBase() . '.index', [$model]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->model(true);

        if ($this->requiresAuthorization('view')) {
            $this->authorize('view', $model);
        }

        if (request()->wantsJson()) {
            if (class_exists($resourceClass = $this->resourceClass())) {
                return new $resourceClass($model);
            }
            return $model;
        }
        if ($model->deleted_at) {
            flash($model->singularModelName()." HAS BEEN DELETED. You can no longer make changes.")->error()->important();
        }
        return view($this->templateBase() . '.show', $this->with('show') ?? [])->with($this->parameterName(), $model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $model = $this->model();

        if ($this->requiresAuthorization('update')) {
            $this->authorize('update', $model);
        }

        return view($this->templateBase() . '.edit', $this->with('edit') ?? [])->with($this->parameterName(), $model)->withRequest($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $model = $this->model();

        if ($this->requiresAuthorization('update')) {
            $this->authorize('update', $model);
        }

        $model->update($request->all());

        if (request()->wantsJson()) {
            if (class_exists($resourceClass = $this->resourceClass())) {
                return new $resourceClass($model);
            }
            return $model;
        }

        $params = [$model];

        flash('saved changes')->success();

        if ($request->has('_redirect')) {
            return redirect($request->_redirect);
        }

        return redirect(route($this->routeBase() . '.show', $params));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->model();

        if ($this->requiresAuthorization('delete')) {
            $this->authorize('delete', $model);
        }

        $model->delete();

        $params = [];

        flash($model->singularModelName()." deleted successfully")->success();

        return redirect(route($this->routeBase() . '.index', $params));
    }

    public function bulkUpdate(Request $request)
    {
        $modelClass = $this->modelClass();
        $items = $modelClass::find($request->rows);

        if ($this->requiresAuthorization('index')) {
            foreach ($items as $item) {
                $this->authorize('update', $item);
            }
        }

        $params = collect($request->except('rows'))->filter();

        $items->each(function ($item) use ($params, $request) {
            $item->update($params->toArray());
        });

        flash('Saved changes to rows [ ' . join(', ', $items->map->getRouteKey()->toArray()) . ' ]')->success()->important();

        return redirect()->back();
    }
}
