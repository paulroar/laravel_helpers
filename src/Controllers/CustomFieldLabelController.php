<?php

namespace RoarIT\Controllers;

use Illuminate\Http\Request;
use RoarIT\Models\CustomFieldLabel;

class CustomFieldLabelController extends CrudController
{
    protected function templateBase()
    {
        return 'roar-it::custom-fields';
    }

    protected function parameterName()
    {
        return 'custom_field';
    }

    protected function modelClass()
    {
        return CustomFieldLabel::class;
    }

    public function show($id)
    {
        return $this->edit($id, request());
    }

    protected function requiresAuthorization($method)
    {
        return config('roar-it.custom-fields.authorize');
    }
}
