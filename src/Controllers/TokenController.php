<?php

namespace RoarIT\Controllers;

use Illuminate\Http\Request;
use RoarIT\Tokens\Token;
use Illuminate\Routing\Controller;
use Exception;
use Illuminate\Validation\ValidationException;

class TokenController extends Controller
{
    public function process($token)
    {
        try {
            return Token::from($token)->process() ?? redirect('/');
        } catch (ValidationException $e) {
            throw $e;
        } catch (Exception $e) {
            flash("Oops! - ".$e->getMessage())->error();
        }
        return redirect('/');
    }
}
