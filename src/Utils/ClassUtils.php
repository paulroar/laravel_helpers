<?php

namespace RoarIT\Utils;

use Symfony\Component\Finder\Finder;
use ReflectionClass;

class ClassUtils
{
    public static function getClassesWhichUseTrait($trait)
    {
        $finder = new Finder();
        $finder->files()->in(app_path())->name('/\.php$/')->depth('== 0');

        return collect($finder)->keys()
        ->map(function ($path) {
            if (preg_match("/.*\/(\w+)\.php$/", $path, $matches)) {
                return $matches[1];
            }
            return null;
        })->filter()
        ->map(function ($class) {
            return "App\\" . $class;
        })->filter(function ($class) use ($trait) {
            return class_exists($class) && collect(static::class_uses_deep($class))->contains($trait);
        })->values();
    }

    public static function getMethodNames($class)
    {
        return collect((new ReflectionClass($class))->getMethods())->map->name;
    }


    public static function getTraitMethodNames($class, $targetMethod)
    {
        $res = [];

        foreach (class_uses_recursive($class) as $trait) {
            $method = $targetMethod . class_basename($trait);

            if (method_exists($class, $method)) {
                $res[] = $method;
            }
        }

        return $res;
    }

    public static function class_uses_deep($class, $autoload = true)
    {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));
        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }
        return array_unique($traits);
    }
}
