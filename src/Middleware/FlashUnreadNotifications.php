<?php

namespace RoarIT\Middleware;

use RoarIT\Notifications\FlashMessage;
use Closure;

class FlashUnreadNotifications
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()){
            foreach ($request->user()->unreadNotifications()->whereType(FlashMessage::class)->oldest()->get() as $notification) {
                flash($notification->data['message'])->important();
                $notification->markAsRead();
            }
        }
        return $next($request);
    }
}
