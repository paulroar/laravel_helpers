<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_lines', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('audit_id')->unsigned()->index();

            $table->string('field_name')->index();
            $table->text('old_value')->nullable();
            $table->text('new_value')->nullable();

            $table->foreign('audit_id')->references('id')->on('audits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_lines');
    }
}
