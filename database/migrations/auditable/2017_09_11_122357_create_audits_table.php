<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if(!config('roar-it.packages.auditable')){
        //     return;
        // }
        Schema::create('audits', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->morphs('auditable');
            
            $table->BigInteger('user_id')->unsigned()->nullable()->index();
            $table->nullableMorphs('modifier');
            $table->string('message')->nullable();

            $table->string('event_name')->index();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // if(!config('roar-it.packages.auditable')){
        //     return;
        // }
        Schema::dropIfExists('audits');
    }
}
