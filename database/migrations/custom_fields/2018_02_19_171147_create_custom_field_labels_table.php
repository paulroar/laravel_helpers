<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use RoarIT\Models\CustomField;

class CreateCustomFieldLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_labels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model')->unique();

            $table->timestamps();
        });

        CustomField::seedDesiredColumns('custom_field_labels');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_labels');
    }
}
